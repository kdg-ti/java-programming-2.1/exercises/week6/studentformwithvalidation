package be.kdg.java2.studentformwithvalidation.presentation.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class StudentDTO {
    private static final Logger log = LoggerFactory.getLogger(StudentDTO.class);
    @NotBlank(message = "Name is mandatory")
    @Size(min=3, max=100, message = "Name should have length between 2 and 100")
    private String name;

    @NotNull(message = "Birthday is mandatory")
    @Past(message = "Birthday should be in the past")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    @Max(value=60, message = "You can enter a maximum of 60 credits")
    private int credits;

    @NotBlank(message = "Country is mandatory")
    private String country;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "StudentDTO{" +
                "name='" + name + '\'' +
                ", birthday=" + birthday +
                ", credits=" + credits +
                ", country='" + country + '\'' +
                '}';
    }
}
