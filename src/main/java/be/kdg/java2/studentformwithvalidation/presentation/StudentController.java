package be.kdg.java2.studentformwithvalidation.presentation;

import be.kdg.java2.studentformwithvalidation.presentation.dto.StudentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/student")
public class StudentController {
    private static final Logger log = LoggerFactory.getLogger(StudentController.class);

    @GetMapping()
    public String getStudentForm(Model model){
        model.addAttribute("student", new StudentDTO());
        return "form";
    }

    @PostMapping()
    public String postStudentForm(@Valid @ModelAttribute("student") StudentDTO student, BindingResult errors){
        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error->{
                log.error(error.toString());
            });
            return "form";
        }
        return "studentinfo";
    }
}
