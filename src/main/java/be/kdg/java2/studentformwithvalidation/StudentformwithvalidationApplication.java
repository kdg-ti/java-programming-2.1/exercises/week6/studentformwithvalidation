package be.kdg.java2.studentformwithvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentformwithvalidationApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentformwithvalidationApplication.class, args);
    }

}
